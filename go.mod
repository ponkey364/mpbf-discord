module gitlab.com/ponkey364/mpbf-discord

go 1.16

require (
	github.com/bwmarrin/discordgo v0.23.2
	gitlab.com/ponkey364/mpbf v0.0.0-20210713200100-7d031286d0f9
)
